#include "win_types.hpp"
#define ISLATINKEY(keysym) \
  (((KeySym)(keysym) >= XK_space) && ((KeySym)(keysym) <= XK_asciitilde))

namespace win
{
  void movable_win::on_mouse_move(XMotionEvent & xmotion)
  {
    if (drag == true)
      {
	util::rect win_rect(get_size()),
	  parent_rect(parent.get_local_size());
	util::point new_pointer(xmotion.x_root,xmotion.y_root);
	drag_point += (new_pointer - pointer);
	auto new_pos = drag_point;
	win_rect.set_origin(drag_point);
	pointer = new_pointer;

	if (bounded)
	  {
	    if (new_pos.x < parent_rect.x())
	      new_pos.x = parent_rect.x();
	    else if (win_rect.ur().x > parent_rect.ur().x)
	      new_pos.x = (parent_rect.ur().x - win_rect.width());
	    
	    if (new_pos.y < parent_rect.y())
	      new_pos.y = parent_rect.y();
	    else if (win_rect.bl().y > parent_rect.bl().y)
	      new_pos.y = (parent_rect.bl().y - win_rect.height());
	  }
	
	move(new_pos.x,new_pos.y);
      }
  }
  
  void scaleable_win::on_mouse_move(XMotionEvent & xmotion)
  {
    if (drag == true)
      {
	util::rect win_rect(get_local_size());
	util::point new_pointer(xmotion.x_root,xmotion.y_root),
	  pointer_delta = (new_pointer - pointer);
	pointer = new_pointer;

	if (((int)win_rect.width() + pointer_delta.x) > 10)
	  win_rect.width() += pointer_delta.x;
	else
	  win_rect.width() = 10;

	if (((int)win_rect.height() + pointer_delta.y) > 10)
	  win_rect.height() += pointer_delta.y;
	else
	  win_rect.height() = 10;

	resize(win_rect.width(), win_rect.height());
      }
  }
  
  void text_input_win::on_key_press(KeySym key, mod_t mod, XKeyEvent & xkey)
  { 
    if (key == XK_BackSpace)
      {
	if (!text_buffer.empty())
	  text_buffer.pop_back();
      }
    else if (key == XK_Return)
      {
	text_return = true;
	on_return();
      }
    else if (ISLATINKEY(key))
      {
	KeySym k;
	XLookupString(&xkey, current_char, char_buff_len, &k, NULL);
	
	text_buffer += current_char;
      }
    set_exposed();
  }

  void multifunction_win::on_key_press(KeySym key, win::mod_t mod, XKeyEvent & xkey)
  {
    KeySym lower, upper;
    XConvertCase(key, &lower, &upper);
    
    auto args = generate_args();

    c_list.call_action(lower, mod, args);
  }
  
}
