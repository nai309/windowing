#ifndef GC_HPP
#define GC_HPP
#include "windows.hpp"

namespace excep
{
  //! Exception thrown when a graphic context could not be created.
  class no_gc : public std::exception {};
}

/**
 * Namespace for classes and functions relating to using xlib graphics contexts.
 */
namespace x_gc
{
  /**
   * Wrapper class for X11 graphics context.
   */
  class graphics_context
  {
  private:
    win::x_display * display;
    win::window & window;
    GC gc = 0;
  public:
    graphics_context(win::window & w);
    ~graphics_context();

    //! Sets the background color of the graphics context.
    void set_background(const win::color & color);
    //! Sets the foreground color of the graphics context.
    void set_foreground(const win::color & color);

    //! Clears the graphics context area so that it can be redrawn. \bug why is this here?
    void clear_area(const util::rect & r, const bool expose);
    //! Clears the entire entire graphics context \bug Why is this here?
    void clear_window();

    //! Draws a line from the point \c p1 to the point \c p2.
    void draw_line(const util::point & p1, const util::point & p2);
    //! Draws a rectangle.
    void draw_rect(const util::rect & r);
    //! Draws a filled rectangle.
    void fill_rect(const util::rect & r);
    /** @brief Draws a text string in the graphics context.
     * @param origin Position of the lower left corner of the first character.
     * @param message String to be drawn.
     */
    void draw_text(const util::point & origin, const std::string & message);
    //! Gets the space used by a text string based on the current graphics context.
    util::rect get_text_rect(const std::string & message);
    //! Gets the maximum hight of text in the font of the current graphics context.
    int get_max_text_height();
    operator GC() { return gc; }
  };

  /**
   * Derived window class containing an X11 graphics context.
   */
  class gc_window : virtual public win::window
  {
  protected:
    //! \bug Why is this a pointer.
    graphics_context * gc;
  public:
    gc_window(std::string new_title = "window",
	      util::rect size = win::def_win_rect,
	      win::x_display * dis = win::def_display())
      : window(new_title,size,dis)
    {
      gc = new graphics_context(*this);
    }
    gc_window(window & parent, util::rect size = win::def_win_rect)
      : window(parent, size)
    {
      gc = new graphics_context(*this);
    }
    virtual ~gc_window() { delete gc; }
  };
  
}


#endif //GC_HPP
