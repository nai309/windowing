#ifndef CONTROLS_HPP
#define CONTROLS_HPP
#include <vector>
#include <unordered_map>
#include <functional>

#include "X11/Xlib.h"
#include "X11/keysym.h"
#include "X11/Xutil.h"
#include "windows.hpp"

/**
 * Namespace containing classes related to the keyboard control system.
 */
namespace cont
{
  /** @brief Structure to hold the arguments that can be passed to an action function.
   *
   * @todo replace placeholder code with functional implementation
   */
  struct arg_list
  {
    int place_holder = 0;

    arg_list(int pl) : place_holder(pl) {}
  };

  //! Typedef for the \c function object type used in a control list.
  typedef std::function<int(arg_list &)> faction;

  //! Structure containing the name and associated function of an action.
  struct action_id
  {
    std::string name;
    faction action;

    action_id(const std::string & n, faction a)
      : name(n), action(a) {}
  };

  //! structure for holding the key and modifier state of a key bind.
  struct key_bind
  {
    KeySym key;
    win::mod_t mod;

    key_bind(const KeySym & key, const win::mod_t & mod)
      : key(key), mod(mod) {}
    bool operator==(const key_bind k)
    { return ((key == k.key) && (mod == k.mod)); }
  };
  
  //! Structure for the element of a \c cvect list used to generate a \c control_list.
  struct control
  {
    std::string name;
    faction action;
    std::vector<key_bind> binds;

    /** @brief Constructor for the \c control structure.
     *
     * @param n name of the control action
     * @param a function to be associated with the action
     * @param k key for the default binding of the action
     * @param m modifier state for the default binding of the action
     */
    control(const std::string & n, const faction & a, const KeySym  & k, const win::mod_t & m)
      : name(n), action(a)
    {
      binds.push_back({k, m});
    }
    /** @brief Constructor for the \c control structure.
     *
     * @param n name of the control action
     * @param a function to be associated with the action
     * @param b key bind for default binding
     */
    control(const std::string & n, const faction & a, const key_bind & b)
      : name(n), action(a)
    {
      binds.push_back(b);
    }
    /** @brief Constructor for the \c control structure.
     *
     * @param n name of the control action
     * @param a function to be associated with the action
     * @param bs list of key binds for default bindings
     */
    control(const std::string & n, const faction & a, const std::vector<key_bind> & bs)
      : name(n), action(a), binds(bs) {}
  };

  //! Typedef for the list of actions and default bindings used to generate a \c control_list.
  typedef std::vector<control> cvect;



  //! Functor for determining equivalency of keybinds.
  class kequal
  {
  public:
    bool operator()(const key_bind & k1, const key_bind & k2) const
    {
    return ((k1.key == k2.key) && (k1.mod == k2.mod));
    }
  };

  //! Functor for creating a hash of a key combination.
  struct key_hash
  {
    std::size_t operator()(key_bind const k) const
    {
      auto h1 = std::hash<KeySym>{}(k.key);
      auto h2 = std::hash<win::mod_t>{}(k.mod);

      return h1 ^ (h2 << 1);
    }
  };

  /**
   * Class defining the mapping between an action function and its set keybind.
   *
   * Supports overriding default bindings, but also supports multiple keybinds for the same action.
   */
  class control_list
  {
  private:
    //! Mapping between the action ids and the action functions.
    std::unordered_map<std::size_t, action_id> actions;
    //! Default binding list created from the \c cvect structure passed into the \c generate_bindings function.
    std::unordered_map<key_bind, std::size_t, key_hash, kequal> def_binds;
    //! List of non default bindings set after generation.
    std::unordered_map<key_bind, std::size_t, key_hash, kequal> binds;
  public:
    /** @brief Populates the action map and the default keybind list.
     *
     * @param controls structure defining the possible actions and their associated default keybinds
     */
    void generate_bindings(const cvect & controls);

    /** @brief Sets keybindings to override those set in the default bind list.
     *
     * @warning This function should not be called more than once during the lifetime of a \c control_list.
     * 
     *
     * @param b list of associated keybinds and actions
     */
    void set_bindings(const std::vector<std::pair<key_bind, std::size_t>> & b);

    //! Prints current list of keybinds to \c stdout.
    void print_bindings();

    /** @brief Executes action associated with the given key combination.
     * 
     * If the key combination is mapped to an action, it is executed.
     * Otherwise, returns without doing anything. 
     *
     * @param k key symbol for the given key combination
     * @param m modifier keys for the given key combination
     * @param arg arguments that can be passed to the action
     */
    void call_action(const KeySym k, const win::mod_t m, arg_list & arg)
    {
      key_bind bind(k,m);
      if (actions.empty())
	{
	  return;
	}
      
      auto end = binds.end();
      auto bind_it = binds.find(bind);
      if (bind_it == end)
	{
	  end = def_binds.end();
	  bind_it = def_binds.find(bind);
	}
      if (bind_it != end)
	{
	  auto act_it = actions.find(bind_it->second);
	  if (act_it != actions.end())
	    {
	      act_it->second.action(arg);
	    }
	}
    }
  };
}


#endif //CONTROLS_HPP
