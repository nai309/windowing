#include "gc.hpp"

namespace x_gc
{
  graphics_context::graphics_context(win::window & w)
    : display(w.get_display()), window(w)
  {
    XGCValues values;

    values.background = 1;

    gc = XCreateGC(display->get_disp(), window.id(), GCBackground, &values);

    if (!gc)
      {
	std::printf("could not create graphics context\n");
	throw excep::no_gc();
      }
  }

  graphics_context::~graphics_context()
  {
    XFreeGC(display->get_disp(), gc);
  }

  void graphics_context::set_background(const win::color & color)
  {
    XSetBackground(display->get_disp(), gc, color.pixel());
  }

  void graphics_context::set_foreground(const win::color & color)
  {
    XSetForeground(display->get_disp(), gc, color.pixel());
  }

  void graphics_context::clear_area(const util::rect &r, const bool expose)
  {
    XClearArea(display->get_disp(), window.id(),
	       r.get_x(), r.get_y(),
	       r.get_width(), r.get_height(),
	       expose);
  }

  void graphics_context::clear_window()
  {
    XClearWindow(display->get_disp(), window.id());
  }
  
  void graphics_context::draw_line(const util::point &p1, const util::point &p2)
  {
    XDrawLine(display->get_disp(), window.id(), gc,
	      p1.x, p1.y, p2.x, p2.y);
  }

  void graphics_context::draw_rect(const util::rect & r)
  {
    XDrawRectangle(display->get_disp(), window.id(), gc,
		   r.get_x(), r.get_y(), r.get_width(), r.get_height());
  }

  void graphics_context::fill_rect(const util::rect & r)
  {
    XFillRectangle(display->get_disp(), window.id(), gc,
		   r.get_x(), r.get_y(), r.get_width(), r.get_height());
  }

  void graphics_context::draw_text(const util::point & origin, const std::string & message)
  {
    XDrawString(display->get_disp(), window.id(), gc,
		origin.x, origin.y,
		message.c_str(), message.size());
  }

  util::rect graphics_context::get_text_rect(const std::string & message)
  {
    int direction = 0, font_ascent = 0, font_descent = 0;
    XCharStruct char_struct;

    if (!message.empty())
      {
	XQueryTextExtents(display->get_disp(), XGContextFromGC(gc),
			  message.c_str(), message.size(),
			  &direction, &font_ascent, &font_descent,
			  &char_struct);
	
	return util::rect(0,0,
			  (char_struct.rbearing - char_struct.lbearing),
			  (char_struct.ascent - char_struct.descent));
      }
    else
      return util::rect();
  }

  int graphics_context::get_max_text_height()
  {
    XFontStruct * font = XQueryFont(display->get_disp(), XGContextFromGC(gc));
    
    if (font)
      {
	return font->max_bounds.ascent + font->max_bounds.descent;
      }
    else
      {
	return 0;
      }
  }
}
