#include "settings.hpp"

namespace setting
{  
  void control_category::add_mod(const char & mod_char, win::mod_t & mods)
  {
    switch (mod_char)
    {
    case 'C':
      mods |= win::MOD_CTRL;
      break;
    case 'S':
      mods |= win::MOD_SHIFT;
      break;
    case 'M':
      mods |= win::MOD_ALT;
      break;
    case 's':
      mods |= win::MOD_SUPER;
      break;
    default:
      printf("invalid modifier: %c\n", mod_char);
      break;
    }
  }

  win::mod_t control_category::set_mods(const std::string & s)
  {
    std::string::size_type start = 0, i;
    win::mod_t mods = win::MOD_NONE;

    //may be a confusing structure
    for (i = s.length(); s[i] != '-' && i > start; --i) {}
    const auto & sub = s.substr(start, i+1);
    auto end = sub.length();
    
    if (sub.find("-") != sub.npos)
      for (auto i = start; i < end; ++i)
	{
	  if (sub[i] == '-')
	    continue;
	  
	  add_mod(sub[i], mods);
	}
    
    return mods;
  }

  void control_category::process_item(const std::string & s)
  {
    std::string::size_type n = s.find(" "), kn;
    const std::string & key = s.substr(0,n);
    const std::string & vals = s.substr(n+1);
    KeySym k;
    win::mod_t mods;


    if (names.find(key) == names.end())
      {
	std::printf("invalid item: %s\n", key.c_str());
	return;
      }

    auto h = std::hash<std::string>{}(key);
    auto vals_v = util::break_string(vals, " ");
    for (auto x : vals_v)
      {
	
        mods = set_mods(x);
        kn = x.find_last_of('-');
	if (kn == x.npos) { kn = 0; }
	else { kn++; }
	const std::string & ks = x.substr(kn);
	k = XStringToKeysym(ks.c_str());
	binds.push_back({{k, mods}, h});
      }
  }

  void control_category::generate_config(std::ofstream & of)
  {
    std::string hold_line = "";
    
    for (auto & x : c_vals)
      {
	hold_line = '#';
	hold_line += x.name;

	for (auto & b : x.binds)
	  {
	    hold_line += ' ';
	    
	    if (b.mod & win::MOD_CTRL)
	      {
		hold_line += "C-";
	      }
	    if (b.mod & win::MOD_SHIFT)
	      {
		hold_line += "S-";
	      }
	    if (b.mod & win::MOD_ALT)
	      {
		hold_line += "M-";
	      }
	    if (b.mod & win::MOD_SUPER)
	      {
		hold_line += "s-";
	      }
	    
	    hold_line += XKeysymToString(b.key);
	  }
	of << hold_line << '\n';
      }
  }
  
  std::string settings_register::find_category_name(std::ifstream & file)
  {
    std::string name;
    bool found_cat = false;

    while (found_cat != true)
      {	
	file.ignore(std::numeric_limits<std::streamsize>::max(), '[');
	if (file.eof() == true) { return ""; }
	std::getline(file, name, '\n');
	if (name.back() == ']')
	  {
	    name.pop_back();
	    found_cat = true;
	  }
	if (file.eof() == true) { return ""; }
      }

    return name;
  }
  
  void settings_register::process_settings_file(const std::string & path)
  {
    std::ifstream file(path);
    if (file.fail())
      {
	std::printf("file not found: %s\n", path.c_str());
	return;
      }
    std::string current_name, line;

    while (file.eof() != true)
      {
	current_name = find_category_name(file);
	if (current_name.empty()) { return; }
	if (!set_category(current_name))
	  {
	    std::cout <<
	      "invalid category: " <<
	      current_name <<
	      std::endl;
	    continue;
	  }
	while (file.eof() != true)
	  {
	    std::getline(file, line, '\n');
	    if (line.front() == '[')
	      {
		if (!set_category(line.substr(1, line.length()-2)))
		  {
		    std::cout << "invalid category: " <<
		      line.substr(1, line.length()-2) << std::endl;
		    break;
		  }
		continue;
	      }
	    if (line.front() == '#')
	      {
		continue;
	      }
	    current_cat->process_item(line);
	  }
      }
    
  }

  void settings_register::generate_settings_file(const std::string & path)
  {
    std::ofstream ofile(path);

    if (ofile.is_open() == false)
      {
	std::cout << "failed to open file to generate settings";
	return;
      }
    
    for (auto x : cats)
      {
	ofile << "#[" << x.second->get_name() << "]\n";
	x.second->generate_config(ofile);
      }
  }
}
