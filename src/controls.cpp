#include "controls.hpp"

namespace cont
{
  void control_list::generate_bindings(const cvect & controls)
  {
    actions.reserve(controls.size());
    def_binds.reserve(controls.size());
    for (auto & x : controls)
      {
	std::size_t h = std::hash<std::string>{}(x.name);
	actions.insert({h, action_id(x.name,x.action)});
	for (auto & b : x.binds)
	  {
	    def_binds.insert({key_bind(b.key, b.mod), h});
	  }
      }
  }

  void control_list::set_bindings(const std::vector<std::pair<key_bind, std::size_t> > & b)
  {
    std::vector<key_bind> rem_binds;
    binds.reserve(b.size());
    for (auto & x : b)
      {
	binds.insert(x);
      }

    for (auto & pair : def_binds)
      {
	if(std::any_of(b.begin(), b.end(),
		    [&pair](const std::pair<key_bind, std::size_t> & p)
		       { return pair.second == p.second; }))
	  {
	    rem_binds.push_back(pair.first);
	  }
      }
    for (auto & b : rem_binds)
      {
	def_binds.erase(b);
      }				   
  }

  void control_list::print_bindings()
  {
    key_hash hs;
    for (auto x : def_binds)
      {
	auto h = hs(x.first);
	std::printf("%s, %d\n", XKeysymToString(x.first.key), x.first.mod);
	std::printf("%zu, %zu\n", h, x.second);
      }
    if (!binds.empty())
      {
	for (auto x : binds)
	  {
	    auto h = hs(x.first);
	    std::printf("%s, %d\n", XKeysymToString(x.first.key), x.first.mod);
	    std::printf("%zu, %zu\n", h, x.second);
	  }
      }
  }
  
}
