#ifndef WIN_TYPES_HPP
#define WIN_TYPES_HPP
#include <string>
#include "windows.hpp"
#include "controls.hpp"

namespace win
{
  /**
   * Window that can be moved around by mouse dragging.
   */
  class movable_win : virtual public window
  {
  private:
    bool drag = false;
    bool bounded = true;
    util::point drag_point, pointer;
  public:
    using window::window;
    
    void move_drag_set(XButtonEvent & xbutton, bool state)
    {
      drag = state;
      if (drag)
	{
	  drag_point = get_pos();
	  pointer = util::point(xbutton.x_root,xbutton.y_root);
	}
    }
        
    virtual void on_mouse_move(XMotionEvent & xmotion);

    void set_bounded(const bool toggle) { bounded = toggle; }
  };

  /**
   * Window that can be resized on mouse drag.
   */
  class scaleable_win : virtual public window
  {
  private:
    bool drag = false;
    util::point pointer;
  public:
    using window::window;

    void scale_drag_set(XButtonEvent & xbutton, bool state)
    {
      drag = state;
      if (drag)
	{
	  pointer = util::point(xbutton.x_root,xbutton.y_root);
	}
    }

    virtual void on_mouse_move(XMotionEvent & xmotion);
  };
  
  class text_input_win : virtual public window
  {
  protected:
    std::string text_buffer;
    char current_char[16];
    int char_buff_len = 16;
    bool text_return = false;
  public:
    using window::window;

    virtual void on_key_press(KeySym key, mod_t mod, XKeyEvent & xkey);

    virtual void on_return() {};
  };

  /**
   * Window that can uses a \c control_list to define various functionality.
   */
  class multifunction_win : virtual public window
  {
  protected:
    cont::control_list c_list;
  public:
    multifunction_win() {}

    cont::control_list & controls() { return c_list; }

    virtual void on_key_press(KeySym key, win::mod_t mod, XKeyEvent & xkey);

    virtual cont::arg_list generate_args() { return cont::arg_list(0); }
  };
  
}

#endif //WIN_TYPES_HPP
