#include <cstdio>
#include <vector>
#include <string>
#include <iostream>
#include "windows.hpp"
#include "win_types.hpp"
#include "gc.hpp"
#include "widgets.hpp"
#include "imlib.hpp"


class ev_win : public x_gc::gc_window
{
private:
  win::event_dispatcher * ev_d = nullptr;
public:
  ev_win(std::string new_title, util::rect size = win::def_win_rect)
    : window(new_title, size) {}

  void set_dispatcher(win::event_dispatcher * ev)
  {
    ev_d = ev;
  }
  
  void on_render()
  {
    util::rect box(get_local_size().get_center() - util::point(50,50), 100, 100);
    gc->draw_rect(box);
  }
  
  virtual void on_key_press(KeySym key, win::mod_t mod, XKeyEvent & xkey)
  {
    if (key == 'q' && (mod == win::MOD_NONE))
      {
	if (ev_d != nullptr)
	  ev_d->stop();
      }
  }
};

class test_win : public x_gc::gc_window, public win::movable_win, public win::scaleable_win
{
private:
  win::color border_in= {"red"}, border_out = {"blue"};
public:
  test_win(window & parent, util::rect size)
    : window(parent, size), gc_window(parent, size)
  {
    gc->set_foreground({"white"});
  }
  
  virtual void on_mouse_move(XMotionEvent & xmotion)
  {
    movable_win::on_mouse_move(xmotion);
    scaleable_win::on_mouse_move(xmotion);
  }

  virtual void on_mouse_enter(XCrossingEvent & xenter) { set_border(border_in); }

  virtual void on_mouse_leave(XCrossingEvent & xleave) { set_border(border_out); }

  virtual void on_key_press(KeySym key, win::mod_t mod, XKeyEvent & xkey)
  {
    if (key == XK_f && (mod == win::MOD_NONE))
      {
	util::rect win_rect = get_size();

	std::printf("%d, %d, %d, %d\n", win_rect.x(), win_rect.y(), win_rect.width(), win_rect.height());
      }
  }
};

class ev_win2 : public imlib::imlib_window, public win::movable_win
{
private:
  imlib::image buffer;
public:
  ev_win2(window & parent, util::rect size = win::def_win_rect)
    : window(parent,size), imlib_window(parent, size), buffer(size) {}

  imlib::image & im() { return buffer; }
  
  virtual void on_left_button_down(XButtonEvent & xbutton)
  {
    buffer.fill_rect({xbutton.x-10,xbutton.y-10,20,20}, {200});
    //updates = imlib_updates
    set_exposed();
  }
  
  virtual void on_key_press(KeySym key, win::mod_t mod, XKeyEvent & xkey)
  {
    if (key == 'r' && (mod == win::MOD_NONE))
      {
	try
	  {
	    buffer.load_image_scaled("/home/reign/Pictures/t_imgs/xy_function.png", {{0,0}, get_size().size()});
	  }
	catch (char const *)
	  {
	    std::printf("failed to load image\n");
	  }
	set_exposed();
      }
    else if (key == 'q' && (mod == win::MOD_NONE))
      {
	hide();
      }
  }
  virtual void on_render()
  {
    buffer.render(screen);
  }
};

class fuck_button : public widget::button
{
public:
  fuck_button(win::window & parent,
	      const util::rect size)
    : window(parent, size), button(parent, size) {}

  virtual void on_pressed()
  {
    std::printf("fuck!\n");
  }
};

win::x_display main_display;
win::event_dispatcher events(&main_display);

imlib::imlib_env environment(main_display);

int main()
{
  win::set_def_display(&main_display);
  ev_win main_win("test", {120,60,640,480});
  win::event_handle main_handle(&main_win, &events);
  main_win.set_dispatcher(&events);
  
  main_win.set_background({"gray"});
  main_win.show();
  
  ev_win2 sub_win(main_win, {30,30,320,240});
  win::event_handle sub_handle(&sub_win, &events);
  test_win sub2_win(main_win, {30,30,320,240});
  win::event_handle sub2_handle(&sub2_win, &events);
  sub2_win.set_border({"blue"}, 2);

  auto fb = new fuck_button(sub2_win, {20,20,80,35});
  fb->show();
  
  sub_win.im().fill_image({10,175,10});
  try
    {
      sub_win.im().load_image_scaled("/home/reign/Pictures/t_imgs/xy_function.png", {{0,0}, sub_win.get_size().size()});
    }
  catch (excep::no_image)
    {
      std::printf("failed to load image\n");
    }

  sub_win.im().fill_polygon(imlib::polygon({{40,50},{80,50},{60,80}}));

  sub_win.im().draw_text({40,80}, "Fuck's just a word.\nIf I say fuck.", {200});
  
  sub_win.show();
  
  std::printf("anus\n");

  main_win.on_render();

  events.run();

  //sub_win.hide();
  sub2_win.show();
  
  events.run();

  delete fb;
  
  //events.run();
  
  main_win.hide();

  return 0;
}
