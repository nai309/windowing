#include "windows.hpp"
#include <unistd.h>
#include <cstring>

extern "C"
{
  void libwindowing_is_present(void){};
}

namespace win
{
  x_display * default_display = nullptr;

  void set_def_display(x_display * disp)
  {
    default_display = disp;
  }

  x_display * def_display()
  {
    return default_display;
  }

  static const int event_mask = ExposureMask | 
    ButtonPressMask | 
    ButtonReleaseMask |
    EnterWindowMask | 
    LeaveWindowMask |
    PointerMotionMask |
    FocusChangeMask |
    KeyPressMask |
    KeyReleaseMask |
    SubstructureNotifyMask |
    StructureNotifyMask |
    SubstructureRedirectMask;
  
  mod_t convert_mod(const unsigned int state)
  {
  unsigned int san_modifier = (state);
  mod_t mod = MOD_NONE;
  
  if (san_modifier & ShiftMask)
    mod |= MOD_SHIFT;
  if (san_modifier & ControlMask)
    mod |= MOD_CTRL;
  if (san_modifier & Mod1Mask)
    mod |= MOD_ALT;
  if (san_modifier & Mod4Mask)
    mod |= MOD_SUPER;
  
  return mod;
  }
  
  x_display::x_display()
  {
    disp = XOpenDisplay(NULL);
    if (disp == nullptr)
      {
	std::printf("Could not open X display\n");
        throw excep::no_display();
      }

    vis = DefaultVisual(disp, DefaultScreen(disp));
    depth = DefaultDepth(disp, DefaultScreen(disp));
    cm = DefaultColormap(disp, DefaultScreen(disp));
    
    rec.set_size(WidthOfScreen(DefaultScreenOfDisplay(disp)),
		   HeightOfScreen((DefaultScreenOfDisplay(disp))));
    
  }

  x_display::~x_display()
  {
    XCloseDisplay(disp);
  }

  window::window(std::string new_title, util::rect size, x_display * dis)
    : display(dis), parent(*this)
  {   
    screen = XCreateSimpleWindow(display->get_disp(),
				 DefaultRootWindow(display->get_disp()),
				 size.get_x(), size.get_y(),
				 size.get_width(), size.get_height(),
				 0, 0, 0);

    set_title(new_title);
    
    XFlush(display->get_disp());
  }

  window::window(window & parent_win, util::rect size)
    : display(parent_win.display), parent(parent_win)
  {
    screen = XCreateSimpleWindow(display->get_disp(), parent.id(),
				 size.get_x(), size.get_y(),
				 size.get_width(), size.get_height(),
				 0, 0, 0);

    XFlush(display->get_disp());
  }
  
  window::~window()
  {    
    XDestroyWindow(display->get_disp(), screen);
  }

  void window::set_focus()
  {
    XSetInputFocus(display->get_disp(), screen, RevertToParent, CurrentTime);
    refresh();
  }

  void window::show()
  {
    XSelectInput(display->get_disp(), screen, event_mask);

    on_show();
    XMapWindow(display->get_disp(), screen);
    refresh();
  }
  
  void window::hide()
  {
    XUnmapWindow(display->get_disp(), screen);
    on_hide();
    XFlush(display->get_disp());
  }  
  
  void window::set_title(std::string & new_title)
  {
    //XStoreName(display, screen, new_title.c_str());
    XChangeProperty(display->get_disp(), screen,
		  XInternAtom(display->get_disp(), "WM_NAME", False),
		  XInternAtom(display->get_disp(), "UTF8_STRING", False),
		    8, PropModeReplace, (unsigned char *) new_title.c_str(),
		  new_title.length());
    XChangeProperty(display->get_disp(), screen,
		  XInternAtom(display->get_disp(), "_NET_WM_NAME", False),
		  XInternAtom(display->get_disp(), "UTF8_STRING", False),
		    8, PropModeReplace, (unsigned char *) new_title.c_str(),
		  new_title.length());

    title = new_title;
  }

  util::point window::get_pos() const
  {
    XWindowAttributes atts;
    XGetWindowAttributes(display->get_disp(), screen, &atts);
    return util::point(atts.x,atts.y);
  }

  util::rect window::get_size() const
  {
    XWindowAttributes atts;
    XGetWindowAttributes(display->get_disp(), screen, &atts);
    return util::rect(atts.x, atts.y, atts.width, atts.height);
  }

  util::rect window::get_local_size() const
  {
    XWindowAttributes atts;
    XGetWindowAttributes(display->get_disp(), screen, &atts);
    return util::rect(atts.width, atts.height);
  }

  void window::move(const int x, const int y)
  {
    XMoveWindow(display->get_disp(), screen, x, y);
  }

  void window::move(const util::point & p)
  {
    XMoveWindow(display->get_disp(), screen, p.x, p.y);
  }

  void window::resize(const unsigned int w, const unsigned int h)
  {
    XResizeWindow(display->get_disp(), screen, w, h);
    refresh();
  }

  void window::raise()
  {
    XRaiseWindow(display->get_disp(), screen);
  }

  void window::set_border(const color & color, const unsigned int w)
  {
    XSetWindowBorder(display->get_disp(), screen, color.pixel());

    XSetWindowBorderWidth(display->get_disp(), screen, w);
  }

  void window::set_border(const color & color)
  {
    XSetWindowBorder(display->get_disp(), screen, color.pixel());
  }
  
  void window::set_background(const color & color)
  {    
    XSetWindowBackground(display->get_disp(), id(), color.pixel());
    refresh();
  }


  void event_dispatcher::process_exposed()
  {
    for (auto w : windows)
      {
	if (w->is_exposed())
	  {
	    w->on_render();
	    w->set_unexposed();
	  }
      }
  }

  void event_dispatcher::run()
  {
    XEvent event;

    running = true;

    while(running)
      {
	do
	  {
	    XNextEvent(display->get_disp(), &event);
	    handle_event(event);
	  }
	while (XPending(display->get_disp()));

	process_exposed();

	usleep(16000);
      }
  }

  void event_dispatcher::handle_event(XEvent & event)
  {
    KeySym current_key;
    mod_t mod = MOD_NONE;
    
    for (auto w : windows)
      {
	if (*w == event.xany.window)
	  {
	    switch(event.type)
	      {
	      case Expose:
		w->set_exposed();
		w->on_expose(event.xexpose);
		break;

	      case ClientMessage:
		w->on_client_message(event.xclient);
		break;

	      case ButtonPress:
		if (event.xbutton.button == Button1)
		  {
		    w->on_left_button_down(event.xbutton);
		  }
		else if (event.xbutton.button == Button2)
		  {
		    w->on_middle_button_down(event.xbutton);
		  }
		else if (event.xbutton.button == Button3)
		  {
		    w->on_right_button_down(event.xbutton);
		  }
		else if (event.xbutton.button == Button4)
		  {
		    w->on_scroll_up(event.xbutton);
		  }
		else if (event.xbutton.button == Button5)
		  {
		    w->on_scroll_down(event.xbutton);
		  }
		break;
		
	      case ButtonRelease:
		if (event.xbutton.button == Button1)
		  {
		    w->on_left_button_up(event.xbutton);
		  }
		else if (event.xbutton.button == Button2)
		  {
		    w->on_middle_button_up(event.xbutton);
		  }
		else if (event.xbutton.button == Button3)
		  {
		    w->on_right_button_up(event.xbutton);
		  }
		break;

	      case MotionNotify:
		w->on_mouse_move(event.xmotion);
		break;

	      case EnterNotify:
		w->on_mouse_enter(event.xcrossing);
		break;
		
	      case LeaveNotify:
		w->on_mouse_leave(event.xcrossing);
		break;
		
	      case KeymapNotify:
		XRefreshKeyboardMapping(&event.xmapping);
		break;
		
	      case KeyPress:
	      case KeyRelease:
		{
		  mod = convert_mod(event.xkey.state);
		  XLookupString(&event.xkey, NULL, 0, &current_key, NULL);
		  if (event.type == KeyPress)
		    w->on_key_press(current_key, mod, event.xkey);
		  else
		    w->on_key_release(current_key, mod, event.xkey);
		}
		break;
		
	      default:
		break;
	      }
	  }
      }
  }
  
}
