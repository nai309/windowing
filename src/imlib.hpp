/** @file imlib.hpp
 * 
 * Contains declarations and class definitions for Imlib2 based rendering and windows.
 *
 */
#ifndef IMLIB_HPP
#define IMLIB_HPP

#include <Imlib2.h>
#include "windows.hpp"

#define IMGCACHE 50
#define FONTCACHE 4
#define DEFAULT_FONT_LOC "/usr/share/feh/fonts"
#define DEFAULT_FONT "yudit/12"

extern "C"
{
  void libwindowingrender_is_present(void);
}

namespace excep
{
  //! Exception thrown when a file cannot be loaded by Imlib2.
  class no_image : public std::exception {};
}

/**
 * Namespace containing classes and functions related to doing rendering with Imlib2.
 */
namespace imlib
{
  struct rgb_color
  {
    int r,g,b,a;

    rgb_color(const int r = 0,
	      const int g = 0,
	      const int b = 0,
	      const int a = 255)
      : r(r), g(g), b(b), a(a) {}
  };

  static const rgb_color rgb_white(255,255,255,255);

  [[maybe_unused]] static inline void set_context_color(const rgb_color & c)
  { imlib_context_set_color(c.r, c.g, c.b, c.a); }

  /**
   * Wrapper class for an \c ImlibPolygon structure.
   */
  class polygon
  {
  private:
    ImlibPolygon poly;
  public:
    //! Default constructor for the \c polygon class.
    polygon() { poly = imlib_polygon_new(); }
    //! Constructor that initializes the polygon with a series of points.
    polygon(const std::vector<util::point> & ps)
    {
      poly = imlib_polygon_new();
      add_points(ps);
    }
    ~polygon() { imlib_polygon_free(poly); }

    //! Adds a single point to the polygon.
    void add_point(util::point & p) { imlib_polygon_add_point(poly, p.x, p.y); }
    //! Adds a series of points to the polygon.
    void add_points(const std::vector<util::point> & ps);
    //! Returns the raw \c ImlibPolygon.
    operator ImlibPolygon() const { return poly; }
  };

  /**
   * Class for initializing and modifying the global Imlib2 context.
   */
  class imlib_env
  {
  private:
    Imlib_Font def_font;
    win::x_display & display;
  public:
    imlib_env(win::x_display & dis);
    ~imlib_env();

    Imlib_Font get_def_font() { return def_font; }
  };

  /** @brief Scales the \c src rectangle so it fits in the \c dest rectangle
   * while maintaining its proportions.
   *
   * @param src rectangle to be scaled
   * @param dest rectangle to fit \c src inside of
   * @return a rectangle matching the proportions of \c src that fits inside of \c dest
   */
  util::rect scale_image(const util::rect & src, const util::rect & dest);

  /**
   * Wrapper class for the \c Imlib_Image structure.
   *
   * @todo Add non-scaled image loading.
   */
  class image
  {
  protected:
    Imlib_Image buffer;
    util::rect img_size;
  public:
    //! Constructs image object with given dimensions.
    image(const util::rect & size) : img_size(0,0,size.get_width(),size.get_height())
    {
      buffer = imlib_create_image(size.get_width(), size.get_height());
    }
    //! \overload image(const util::rect & size)
    image(const int w, const int h) : img_size(0,0,w,h)
    {
      buffer = imlib_create_image(w, h);
    }
      
    ~image()
    {
      imlib_context_set_image(buffer);
      imlib_free_image();
    }

    //! Gets the size of the \c image.
    util::rect get_size() const { return img_size; }
    //! Draws a line between two points on the image.
    void draw_line(const util::point & p1, const util::point & p2,
		   rgb_color color = {});
    //! Draws a series of unconnected lines on the image.
    void draw_lines(const std::vector<std::pair<util::point &, util::point &>> & lines, const rgb_color color = {});
    //! Draws a rectangle on the image.
    void draw_rect(const util::rect & r, const rgb_color color = {});
    //! Draws a solid rectangle on the image.
    void fill_rect(const util::rect & r, const rgb_color color = {});
    //! Fills the entire area of the image with a color.
    void fill_image(const rgb_color color = rgb_white);
    /** @brief Draws strings of text on the image.
     *
     * @note This function will draw each line of text in \c v on its own line.
     *
     * @param p top left point of the first line of text
     * @param v list of strings to draw
     * @param color color to use for the drawn text
     */
    void draw_text(const util::point & p, const std::vector<std::string> & v, const rgb_color color = {});
    /** @brief Draws strings of text on the image and returns the resultant size.
     *
     * @note This function will draw each line of text in \c v on its own line.
     *
     * @param p top left point of the first line of text
     * @param v list of strings to draw
     * @param color color to use for the drawn text
     * @return rectangle equal to the total size of all the drawn text
     */
    util::rect draw_text_return(const util::point & p, const std::vector<std::string> & v, const rgb_color color = {});
    /** @brief Draws a string of text on the image.
     *
     * @param p top left point of the line of text
     * @param s string of text to draw
     * @param color color to use for the drawn text
     */
    void draw_text(const util::point & p, const std::string & s, const rgb_color color = {});
    /** @brief Draws a string of text on the image and returns the resultant size.
     *
     * @param p top left point of the line of text
     * @param s string of text to draw
     * @param color color to use for the drawn text
     * @return rectangle equal to the total size of the drawn text
     */
    util::rect draw_text_return(const util::point & p, const std::string & s, const rgb_color color = {});
    /** @brief Finds the size of a text string.
     *
     * @param s string of text to find size of
     * @return rectangle equal to the total size of the drawn text
     */
    util::rect get_text_size(const std::string & s);
    /** @brief Draws a polygon on the image.
     * 
     * @param pol polygon to draw
     * @param color color to use for the drawn polygon
     * @param closed boolean specifying whether the polygon should be closed or left open
     */
    void draw_polygon(const polygon & pol, rgb_color color = {}, bool closed = false);
    /** @brieft Draws a solid polygon on the image.
     *
     * @param pol polygon to draw
     * @param color color to use for the drawn polygon
     */
    void fill_polygon(const polygon & pol, rgb_color color = {});

    //! Changes the size of the image.
    void resize(const util::rect & size)
    {
      imlib_context_set_image(buffer);
      imlib_free_image();
      buffer = imlib_create_image(size.get_width(), size.get_height());
      img_size = util::rect(0,0,size.get_width(),size.get_height());
    }
    /** @brief Tests if a file is loadable by Imlib2.
     *
     * This static function tests if there is a Imlib2 loader that can
     * successfully load the file.
     *
     * @param path path to the file to be tested.
     * @return boolean indicating whether the file can be loaded.
     */
    static bool test_image(const std::string & path);
    /** @brief Load a file into the image scaled down to a given size.
     *
     * This function will scale the loaded image so it fits inside of the \c dest
     * rectangle while maintaining its proportions.
     *
     * @param path path to the file to load
     * @param dest rectangle to scale the loaded image to
     */
    void load_image_scaled(const std::string & path, const util::rect & dest);
    //! Render the image onto a drawable.
    void render(Drawable & d, const util::point & origin = {0,0});
    //! Returns the raw \c Imlib_Image struture.
    operator Imlib_Image() const { return buffer; }
  };

  /**
   * Derived window class for doing Imlib2 based rendering.
   */
  class imlib_window : virtual public win::window
  {
  protected:
    Imlib_Updates updates;
  public:
    imlib_window(std::string new_title = "window",
		 util::rect size = win::def_win_rect,
		 win::x_display * dis = win::def_display());
    imlib_window(win::window & parent, util::rect size = win::def_win_rect);
    ~imlib_window();

    void on_expose(XExposeEvent & xexpose);
  };
   
}


#endif //IMLIB_HPP
