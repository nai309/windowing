#include <vector>
#include "windows.hpp"
#include "win_types.hpp"
#include "controls.hpp"
#include "settings.hpp"

win::x_display main_display;
win::event_dispatcher events(&main_display);
setting::settings_register reg;

int test_print(cont::arg_list & args)
{
  std::printf("test\n");
  return 0;
}

int test_print2(cont::arg_list & args)
{
  std::printf("anus\n");
  return 0;
}

class test_win : public win::multifunction_win
{
private:
  win::event_dispatcher * ev_d = nullptr;
public:
  test_win(const cont::cvect & controls)
    : window()
  {
    c_list.generate_bindings(controls);
    set_background({"gray"});
  }

  void set_dispatcher(win::event_dispatcher * ev)
  {
    ev_d = ev;
  }

  void on_key_press(KeySym key, win::mod_t mod, XKeyEvent & xkey)
  {
    if (key == XK_q && (mod == win::MOD_NONE))
      {
	if (ev_d != nullptr)
	  ev_d->stop();
      }
    else
      {
	multifunction_win::on_key_press(key, mod, xkey);
      }
  }
};

static const cont::cvect controls =
    {
      {"test", test_print, XK_f, win::MOD_NONE},
      {"test2", test_print2, XK_f, win::MOD_CTRL},
      {"test3", [](cont::arg_list & args){ std::printf("lambda\n"); return 4; }, XK_t, win::MOD_NONE}
    };

setting::control_category main_controls("main", controls);

class t2_win : public win::movable_win, public win::multifunction_win
{
private:

public:
  t2_win(const cont::cvect & controls, win::window & parent, const util::rect size)
    : window(parent, size), movable_win(parent,size)
  {
    c_list.generate_bindings(controls);
    set_border({"white"}, 1);
  }

  virtual void on_key_press(KeySym key, win::mod_t mod, XKeyEvent & xkey)
  {
    if (key == XK_r && mod == win::MOD_NONE)
      {
        auto pos = get_pos();
	std::printf("%d, %d\n", pos.x, pos.y);
      }
    else
      {
	multifunction_win::on_key_press(key, mod, xkey);
      }
  }
  
};

int main()
{
  set_def_display(&main_display);
  test_win main_win(controls);
  win::event_handle main_handle(&main_win, &events);
  main_win.set_dispatcher(&events);
  reg.add_category(main_controls);
  reg.process_settings_file("/home/reign/Documents/gits/settings_test.conf");
  main_win.controls().set_bindings(main_controls.return_binds());
  main_win.controls().print_bindings();
  //t2_win sub_win(controls, main_win, {50,50,240,100});

  //auto h = std::hash<std::string>{}("test");
  //std::vector<std::pair<cont::key_bind, std::size_t>> d = {{{XK_x, win::MOD_NONE}, h}, {{XK_z, win::MOD_NONE}, h}};

  //main_win.get_controls().set_bindings(d);
  
  main_win.show();

  events.run();

  //sub_win.show();

  //events.run();
}
