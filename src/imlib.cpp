#include "imlib.hpp"

extern "C"
{
  void libwindowingrender_is_present(void){};
}

namespace imlib
{

  void polygon::add_points(const std::vector<util::point> & ps)
  {
    for (auto p : ps)
      {
	imlib_polygon_add_point(poly, p.x, p.y);
      }
  }

  imlib_env::imlib_env(win::x_display & dis) : display(dis)
  {
    imlib_set_cache_size(IMGCACHE * 1024 * 1024);
    imlib_set_font_cache_size(FONTCACHE * 1024 * 1024);
    imlib_set_color_usage(128);
    imlib_context_set_dither(1);
    imlib_context_set_anti_alias(1);
    imlib_context_set_display(display);
    imlib_context_set_visual(display.get_vis());
    imlib_context_set_colormap(display.get_cm());

    imlib_add_path_to_font_path(DEFAULT_FONT_LOC);
    def_font = imlib_load_font(DEFAULT_FONT);
    imlib_context_set_font(def_font);
  }

  imlib_env::~imlib_env()
  {
    imlib_context_set_font(def_font);
    imlib_free_font();
  }

  util::rect scale_image(const util::rect & src, const util::rect & dest)
  {
    util::rect scale_rect;
    double ratio = (((double) src.get_width()) / ((double) src.get_height()) /
		    ((double) dest.get_width() / (double) dest.get_height()));

    if (ratio > 1.0)
      {
	scale_rect.x() = 0;
	scale_rect.width() = dest.get_width();
	scale_rect.height() = (int) ((double) dest.get_height() / ratio);
	scale_rect.y() = ((int) ((double) dest.get_height() / 2.0)
			  - ((double) scale_rect.height() / 2.0));
      }
    else
      {
	scale_rect.y() = 0;
	scale_rect.width() = (int) ((double) dest.get_width() * ratio);
	scale_rect.height() = dest.get_height();
	scale_rect.x() = ((int) ((double) dest.get_width() / 2.0)
			  - ((double) scale_rect.width() / 2.0));
      }

    return scale_rect;
  }

  
  void image::draw_line(const util::point & p1, const util::point & p2, rgb_color color)
  {
    imlib_context_set_image(buffer);
    set_context_color(color);
    imlib_image_draw_line(p1.x, p1.y, p2.x, p2.y, 0);
  }

  void image::draw_lines(const std::vector<std::pair<util::point &, util::point &>> & lines, const rgb_color color)
  {
    imlib_context_set_image(buffer);
    set_context_color(color);

    for (unsigned int i = 0; i < lines.size(); ++i)
      {
	imlib_image_draw_line(lines[i].first.x, lines[i].first.y,
			      lines[i].second.x, lines[i].second.y, 0);
      }
  }

  void image::draw_rect(const util::rect & r, const rgb_color color)
  {
    imlib_context_set_image(buffer);
    set_context_color(color);
    imlib_image_draw_rectangle(r.get_x(), r.get_y(),
			       r.get_width(), r.get_height());
  }

  void image::fill_rect(const util::rect & r, const rgb_color color)
  {
    imlib_context_set_image(buffer);
    set_context_color(color);
    imlib_image_fill_rectangle(r.get_x(), r.get_y(),
			       r.get_width(), r.get_height());
  }

  
  void image::fill_image(const rgb_color color)
  {
    imlib_context_set_image(buffer);
    set_context_color(color);
    imlib_image_fill_rectangle(0, 0,
			       imlib_image_get_width(),
			       imlib_image_get_height());
  }

  void image::draw_text(const util::point & p, const std::vector<std::string> & v, const rgb_color color)
  {
    util::point pp(p), tp1, tp2;
    imlib_context_set_image(buffer);
    set_context_color(color);
    
  for (auto x : v)
    {
      imlib_text_draw_with_return_metrics(pp.x, pp.y, x.c_str(),
					  &tp1.x, &tp1.y,
					  &tp2.x, &tp2.y);
      pp.y += tp2.y;
    }
  }

  util::rect image::draw_text_return(const util::point &p, const std::vector<std::string> & v, const rgb_color color)
  {
    util::point pp(p), tp1, tp2;
    util::rect ret_rect(p,0,0);
    imlib_context_set_image(buffer);
    set_context_color(color);

    for (auto x : v)
      {
	imlib_text_draw_with_return_metrics(pp.x, pp.y, x.c_str(),
					    &tp1.x, &tp1.y,
					    &tp2.x, &tp2.y);
	pp.y += tp2.y;
	if ((unsigned int) tp1.x > ret_rect.width())
	  ret_rect.width() = ret_rect.x() + tp1.x;
      }
    pp.y -= tp2.y;
    pp.y += tp1.y;
    ret_rect.height() = ret_rect.y() + pp.y;

    return ret_rect;
  }
  
  void image::draw_text(const util::point & p, const std::string & s, const rgb_color color)
  {
    auto v = util::break_string(s, "\n");
    draw_text(p, v, color);
  }

  util::rect image::draw_text_return(const util::point &p, const std::string & s, const rgb_color color)
  {
    auto v = util::break_string(s, "\n");
    return draw_text_return(p, v, color);
  }

  util::rect image::get_text_size(const std::string & s)
  {
    int w = 0, h = 0;
    imlib_context_set_image(buffer);

    imlib_get_text_size(s.c_str(), &w, &h);

    return {w,h};
  }
  
  void image::draw_polygon(const polygon & pol, rgb_color color, bool closed)
  {
    imlib_context_set_image(buffer);
    set_context_color(color);
    imlib_image_draw_polygon(pol, closed);
  }

  void image::fill_polygon(const polygon & pol, rgb_color color)
  {
    imlib_context_set_image(buffer);
    set_context_color(color);
    imlib_image_fill_polygon(pol);
  }

  bool image::test_image(const std::string & path)
  {
    Imlib_Image image = imlib_load_image(path.c_str());
    bool pass = false;

    if (image)
      {
	pass = true;
	imlib_context_set_image(image);
	imlib_free_image();
      }

    return pass;
  }
  
  void image::load_image_scaled(const std::string & path, const util::rect & dest)
  {
    Imlib_Image image = imlib_load_image(path.c_str());
    if (!image)
      {
	throw excep::no_image();
      }

    imlib_context_set_image(image);
    util::rect image_rect(0,0,
			  imlib_image_get_width(),
			  imlib_image_get_height());
    util::rect scale = scale_image(image_rect, dest);

    imlib_context_set_image(buffer);
    imlib_context_set_blend(1);

    imlib_blend_image_onto_image(image, 0,
				 image_rect.x(), image_rect.y(),
				 image_rect.width(), image_rect.height(),
				 (dest.get_x() + scale.x()),
				 (dest.get_y() + scale.y()),
				 scale.width(), scale.height());

    /*updates = imlib_update_append_rect(updates,
				       dest.get_x(), dest.get_y(),
				       scale.width(), scale.height());*/

    imlib_context_set_image(image);
    imlib_free_image();

  }

  void image::render(Drawable & d, const util::point & origin)
  {
    /*updates = imlib_updates_merge_for_rendering(updates,
						get_size().get_width(),
						get_size().get_height());

    */
    
    imlib_context_set_blend(0);
    imlib_context_set_drawable(d);
    imlib_context_set_image(buffer);
    imlib_render_image_on_drawable(0,0);
  }

  imlib_window::imlib_window(std::string new_title,
			     util::rect size, win::x_display * dis)
    : window(new_title, size, dis)
  {
    updates = imlib_updates_init();
  }

  imlib_window::imlib_window(window & parent, util::rect size)
    : window(parent, size)
  {
    updates = imlib_updates_init();
  }

  imlib_window::~imlib_window()
  {
    imlib_updates_free(updates);
  }

  
  void imlib_window::on_expose(XExposeEvent & xexpose)
  {
    updates = imlib_update_append_rect(updates,
				       xexpose.x, xexpose.y,
				       xexpose.width, xexpose.height);
  }
}
