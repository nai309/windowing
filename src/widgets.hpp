#ifndef WIDGETS_HPP
#define WIDGETS_HPP
#include "windows.hpp"
#include "win_types.hpp"
#include "gc.hpp"

/**
 * Namespace for window classes designed to be specific elements in a user program.
 */
namespace widget
{
  /**
   * Window class that defines a button that changes border color on hover,
   * and executes a custom action on press.
   *
   * @todo Make border colors changable
   */
  class button : public x_gc::gc_window
  {
  protected:
    win::color background,
      border_col = {47,47,47,display},
      hover_col = {60,60,60,display};
    bool hover = false,
      pressed = false;
  public:
    //! Constructor for button class.
    button(win::window & parent,
	   const util::rect size = {0,0,108,42},
	   const win::color color = {52,52,52})
      : window(parent, size), gc_window(parent, size), background(color)
    {
      set_background(background);
      set_border(border_col);
      gc->set_background(background);
    }

    virtual void on_mouse_enter([[maybe_unused]] XCrossingEvent & xcrossing)
    {
      hover = true;
      set_border(hover_col);
    }

    virtual void on_mouse_leave([[maybe_unused]] XCrossingEvent & xcrossing)
    {
      hover = false;
      set_border(border_col);
    }

    virtual void on_left_button_down([[maybe_unused]] XButtonEvent & xbutton)
    {
      pressed = true;
      if (hover == true)
	{
	  on_pressed();
	  set_exposed();
	}
    }

    virtual void on_left_button_up([[maybe_unused]] XButtonEvent & xbutton)
    {
      pressed = false;
      set_exposed();
    }

    virtual void on_render()
    {
      gc->clear_window();
    }

    virtual void on_pressed() {};
  };
  
}


#endif //WIDGETS_HPP
