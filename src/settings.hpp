#ifndef SETTINGS_HPP
#define SETTINGS_HPP
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <unordered_set>
#include "windows.hpp"
#include "controls.hpp"

namespace excep
{
  //! Exception thrown when a category is defined twice.
  class existing_category : public std::exception {};
}

/**
 * Namespace for classes and functions related to settings.
 */
namespace setting
{

  /**
   * Base class used for defining settings file categories.
   */
  class category
  {
  protected:
    std::string name = "empty";
  public:
    category(const std::string & s) : name(s) {}
    ~category() {}
    std::string get_name() const { return name; }

    /** @brief Processes a single configuration line.
     * 
     * @param s config line containing a key and value set
     */
    virtual void process_item(const std::string & s) {}

    /** @brief Generates settings section for the category.
     *
     * The implementation should print each default key value pair on a new line, should not include its own category name,
     * and should have each line commented out.
     * Implementation should end every line with a line return character.
     * 
     * @param of \c fstream to output the config to
     */
    virtual void generate_config(std::ofstream & of) {}
  };

  /**
   * Settings category used for customizing keybinds using the controls system.
   * @todo consider removing \c valid_item unused member function
   */
  class control_category : public category
  {
  protected:
    std::unordered_set<std::string> names;
    std::vector<std::pair<cont::key_bind, std::size_t>> binds;
    void add_mod(const char & mod_char, win::mod_t & mods);
    win::mod_t set_mods(const std::string & s);
    const cont::cvect & c_vals;
  public:
    control_category(const std::string & n, const cont::cvect & controls)
      : category(n), c_vals(controls)
    {
      for (auto x : controls)
	{
	  names.insert(x.name);
	}
    }

    bool valid_item(const std::string & s)
    { return (names.find(s) != names.end()); }

    virtual void process_item(const std::string & s);
    virtual void generate_config(std::ofstream & of);
    auto & return_binds() { return binds; }

  };

  /**
   * Class used to register all possible settings categories and process a settings file.
   */
  class settings_register
  {
  private:
    std::unordered_map<std::string, category*>  cats;
    std::string find_category_name(std::ifstream & file);
    category * current_cat = nullptr;
    bool set_category(const std::string & s)
    {
      auto it = cats.find(s);
      if (it != cats.end())
	{
	  current_cat = (it->second);
	  return true;
	}
      else
	return false;
    }
  public:

    /** @brief Adds a category to the register.
     * 
     * @param cat category to be added
     */
    void add_category(category & cat)
    {
      auto success = cats.insert({cat.get_name(),&cat}).second;
      if (!success) { throw excep::existing_category(); }
    }

    /** @brief Processes a settings file based on the rules of all registered categories.
     * 
     * @param path path to the settings file to be read from
     */
    void process_settings_file(const std::string & path);

    /** @brief Generates a settings file with all possible settings keys with their default values.
     *
     * All settings values are entered into the file commented out.
     *
     * @param path path to place the generated file in
     *
     * @todo make failure mode more robust 
     */
    void generate_settings_file(const std::string & path);
  };
  
}

#endif //SETTINGS_HPP
