/** @file windows.hpp
 * 
 * Contains declarations and class definitions for the basic classes used in libwindowing.
 *
 * @todo improve initialization process
 * @todo catch instances of unset default display
 */
#ifndef WINDOWS_HPP
#define WINDOWS_HPP
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/Xutil.h>
#include <string>
#include <vector>
#include <algorithm>
#include <exception>
//#include "config.h"

extern "C"
{
  void libwindowing_is_present(void);
}

/**
 * Namespace for exception types.
 */
namespace excep
{
  //! Exception thrown when an XDisplay cannot be opened.
  class no_display : public std::exception {};
  //! Exception thrown when an XColor cannot be allocated.
  class no_color : public std::exception {};
}

/**
 * Namespace for utiltiy functions and classes.
 */
namespace util
{
  /** @brief Breaks string into tokens along a delimiter.
   *
   * @param s string to break into tokens
   * @param delim delimiter to break string between
   * @return \c vector containing tokens
   */
  [[maybe_unused]] static inline std::vector<std::string> break_string(const std::string & s, const std::string & delim)
  {
    std::vector<std::string> v;
    auto start = 0U;
    auto end = s.find(delim);
    while (end != std::string::npos)
    {
      v.push_back(s.substr(start, end - start));
        start = end + delim.length();
        end = s.find(delim, start);
    }

    v.push_back(s.substr(start, end));

    return v;
  }

  /** @brief 2d point.
   *
   * Structure representing a 2 dimensional point.
   */
  struct point
  {
    int x, y;
    point(int h = 0, int v = 0) : x(h), y(v) {}

    point & operator+=(const point & p)
    { x += p.x; y += p.y; return *this; }
    point & operator-=(const point & p)
    { x -= p.x; y -= p.y; return *this; }
    point & operator*=(const double s)
    { x *= s; y *= s; return *this; }
    point & operator/=(const double s)
    { x /= s; y /= s; return *this; }
  };

  static inline point operator+(const point & p1, const point & p2)
  { return point(p1.x + p2.x, p1.y + p2.y); }

  static inline point operator-(const point & p1, const point & p2)
  { return point(p1.x - p2.x, p1.y - p2.y); }

  static inline point operator*(const point & p, const double s)
  { return point(p.x * s, p.y * s); }

  [[maybe_unused]] static inline point operator*(const double s, const point & p)
  { return (p * s); }

  static inline point operator/(const point & p, const double s)
  { return point(p.x / s, p.y / s); }

  /** @brief 2d rectangle.
   *
   * Structure representing a 2 dimensional rectangle.
   */
  struct rect
  {
    point origin;
    unsigned int w = 0, h = 0;
    
    rect(int x, int y, int w, int h) : origin(x, y), w(w), h(h) {}
    rect(point p1, int w, int h) : origin(p1), w(w), h(h) {}
    rect(int w, int h) : w(w), h(h) {}
    rect(point p1, point p2) : origin(p1), w(p2.x), h(p2.y) {}
    rect() {}
    
    void set_origin(const int x1, const int y1) { origin.x = x1; origin.y = y1; }
    void set_origin(const point & p) { origin = p; }
    void set_size(const int wn, const int hn) { w = wn; h = hn; }
    void set_size(const point & p) { w = p.x; h = p.y; }

    int get_x() const { return origin.x; }
    int get_y() const {return origin.y; }
    unsigned int get_width() const { return w; }
    unsigned int get_height() const { return h; }

    int & x() { return origin.x; }
    int & y() { return origin.y; }
    unsigned int & width() { return w; }
    unsigned int & height() { return h; }
    point size() { return point(w,h); }


    point ul() const { return origin; }
    point ur() const { return point(origin.x + w, origin.y); }
    point bl() const { return point(origin.x, origin.y + h); }
    point br() const { return (origin + point(w,h)); }

    void set_center(const point & p) { origin = p - (point(w,h) / 2); }
    point get_center() const { return (origin + (point(w,h) / 2)); }

    void reflect_x() { origin.x -= w; }
    void reflect_y() { origin.y -= h; }

    rect & operator*=(const double s) { w *= s; h *= s; return *this; }
  };

  static inline rect operator*(const rect & r, const double s)
  { return rect(r.origin, (r.w * s), (r.h * s)); }
  [[maybe_unused]] static inline rect operator*(const double s, const rect & r)
  { return (r * s); }
  static inline rect operator/(const rect & r, const double s)
  { return rect(r.origin, (r.w / s), (r.h / s)); }
  
}

/**
 * Namespace containing functions and classes for general windowing windows.
 */
namespace win
{
  class x_display;
  class window;
  class event_dispatcher;

  //! Default window dimensions.
  static const util::rect def_win_rect = {0,0,640,480};

  /** @brief Bit flags representing modifier keys. 
   */
  enum modifiers {
    MOD_NONE = 0,
    MOD_SHIFT = 1,
    MOD_CTRL = 1 << 1,
    MOD_ALT = 1 << 2,
    MOD_SUPER = 1 << 3
  };

  typedef unsigned int mod_t;

  /** @brief Converts xlib modifier states to windowing modifier states.
   * @param state xlib modifier state
   * @return windowing modifier state
   */
  mod_t convert_mod(const unsigned int state);

  //! Sets the default X display.
  void set_def_display(x_display * disp);
  //! Returns the default display.
  x_display * def_display();
  
  /** 
   * Wrapper class for X11 display and related settings.
   */
  class x_display
  {
  private:
    Display * disp;
    Visual * vis;
    Colormap cm;
    int depth;
    util::rect rec;
  public:
    /** @brief Opens an X11 display and grabs display settings needed 
     * by windowing windows.
     */
    x_display();
    /** @brief Closes X11 display connection
     */
    ~x_display();
    
    Display * get_disp() { return disp; }
    operator Display*() const { return disp; }
    Visual * get_vis() { return vis; }
    Colormap get_cm() const { return cm; }
    int get_depth() const { return depth; }
    util::rect get_size() const { return rec; }
  };

  /** 
   * Wrapper class for XColor variables.
   *
   * @todo add set member overload for named colors
   */
  class color
  {
  private:
    void set_color(const short red, const short green, const short blue)
    {
      col.red = red * 65535 / 255;
      col.green = green * 65535 / 255;
      col.blue = blue * 65535 / 255;
      col.flags = DoRed | DoGreen | DoBlue;
      
      if (!XAllocColor(display->get_disp(), cm, &col))
	{
	  throw excep::no_color();
	}
    }
    void free_color()
    {
      //unsigned long pixels[1];
      //pixels[0] = col.pixel;
      
      //XFreeColors(display, cm, pixels, 1, 0);
    }
  protected:
    XColor col;
    x_display * display;
    Colormap cm;
  public:
    //! Creates color object with a value based on the given RGB values.
    color(const short red, const short green, const short blue, x_display * d = def_display())
      : display(d), cm(d->get_cm())
    { set_color(red, green, blue); }
    //! Creates a color object with a value from the named colors defined by Xlib
    color(const std::string & color_name, x_display * d = def_display())
      : display(d), cm(d->get_cm())
    {
      XColor col_e;
      
      if(!XAllocNamedColor(display->get_disp(), cm, color_name.c_str(), &col, &col_e))
	{
	  throw excep::no_color();
	}
    }
    ~color() { free_color(); }

    //! Sets color value based on RGB values.
    void set(const short red, const short green, const short blue)
    {
      free_color();
      set_color(red, green, blue);
    }
    //! Sets color value based on the value of another color object.
    void set(color & c)
    {
      free_color();
      set_color(c.r(), c.g(), c.b());
    }
 
    long pixel() const { return col.pixel; }
    unsigned short r() const { return col.red * 255 / 65535; }
    unsigned short g() const { return col.green * 255 / 65535; }
    unsigned short b() const { return col.blue * 255 / 65535; }

    color & operator=(color & c)
    { set(c); return *this; }
  };

  /**
   * Base class for windowing windows.
   */
  class window
  {
  protected:
    x_display * display;
    Window screen;
    std::string title;
    window & parent;
    std::vector<Atom> atoms;
    bool exposed = false;
    event_dispatcher * dispatcher = nullptr;
  public:
    //! Creates a top level window in the display.
    window(std::string new_title = "window",
	   util::rect size = def_win_rect,
	   x_display * dis = def_display());
    //! Creates a child window in a parent window.
    window(window & parent, util::rect size = def_win_rect);
    virtual ~window();
    Window id() const { return screen; }
    x_display * get_display() { return display; }
    bool is_top_window() const { return *this == parent; }
    void set_exposed() { exposed = true; }
    void set_unexposed() { exposed = false; }
    bool is_exposed() const { return exposed; }
    void set_dispatcher(event_dispatcher * e) { dispatcher = e; }
    void set_focus();
    //! Sets the window as exposed so its contents can be rerendered.
    void refresh() { /*on_render(); XFlush(display);*/ exposed = true; }
    //! Sets the window visible on the display.
    void show();
    //! Sets the window invisible on the display.
    void hide();
    //! sets the title of the window that appears in window managers.
    void set_title(std::string & new_title);
    //! Gets the position of the top left corner of the window.
    util::point get_pos() const;
    //! Gets the rectangle the window occupies in its parent window.
    util::rect get_size() const;
    //! Gets a rectangle the size of the window with its origin at zero.
    util::rect get_local_size() const;
    //! gets the center point of the window in its parent window.
    util::point get_center() const
    { util::rect sr = get_size() / 2; return sr.br(); }
    //! get the center point of the window if its origin was a zero.
    util::point get_relative_center() const
    { util::rect sr = get_local_size() / 2; return sr.br(); }
    void move(const int x, const int y);
    void move(const util::point & p);
    virtual void resize(const unsigned int w, const unsigned int h);
    void raise();
    operator Window() { return id(); }
    bool operator==(const window & w2) const { return (id() == w2.id()); }
    bool operator==(const Window & wid) const { return (id() == wid); }

    //! Set the color and size of the window border.
    void set_border(const color & color, const unsigned int w);
    //! Set the color of the window border.
    void set_border(const color & color);
    //! Set the color of the window background.
    void set_background(const color & color);

    //! Callback run to execute rendering updates for the window object.
    virtual void on_render() {};
    /** @brief Callback run when an \c Expose event is recieved.
     * 
     * @note This function is not what should be used for rendering, for that use \c on_render.
     */
    virtual void on_expose([[maybe_unused]] XExposeEvent & xexpose) {};

    //! Callback run when \c show is called.
    virtual void on_show() {};
    //! Callback run when \c hide is called.
    virtual void on_hide() {};

    //! Callback run when a client message is recieved
    virtual void on_client_message([[maybe_unused]] XClientMessageEvent & xclient) {};
    //! Callback run when the left mouse button is pressed.
    virtual void on_left_button_down([[maybe_unused]] XButtonEvent & xbutton) {};
    //! Callback run when the right mouse button is pressed.
    virtual void on_right_button_down([[maybe_unused]] XButtonEvent & xbutton) {};
    //! Callback run when the middle mouse button is pressed.
    virtual void on_middle_button_down([[maybe_unused]] XButtonEvent & xbutton) {};

    //! Callback run when the left mouse button is released.
    virtual void on_left_button_up([[maybe_unused]] XButtonEvent & xbutton) {};
    //! Callback run when the right mouse button is released.
    virtual void on_right_button_up([[maybe_unused]] XButtonEvent & xbutton) {};
    //! Callback run when the middle mouse button is released.
    virtual void on_middle_button_up([[maybe_unused]] XButtonEvent & xbutton) {};

    //! Callback run when the scroll wheel moves up.
    virtual void on_scroll_up([[maybe_unused]] XButtonEvent & xbutton) {};
    //! Callback run when the scroll wheel moves down.
    virtual void on_scroll_down([[maybe_unused]] XButtonEvent & xbutton) {};

    //! Callback run when the mouse moves.
    virtual void on_mouse_move([[maybe_unused]] XMotionEvent & xmotion) {};

    //! Callback run when the mouse enters the bounds of the window.
    virtual void on_mouse_enter([[maybe_unused]] XCrossingEvent & xenter) {};
    //! Callback run when the mouse leaves the bounds of the window.
    virtual void on_mouse_leave([[maybe_unused]] XCrossingEvent & xleave) {};

    //! Callback run when a keyboard key is pressed.
    virtual void on_key_press([[maybe_unused]] KeySym key, [[maybe_unused]] mod_t mod, [[maybe_unused]] XKeyEvent & xkey) {};
    //! Callback run when a keyboard key is released.
    virtual void on_key_release([[maybe_unused]] KeySym key, [[maybe_unused]] mod_t mod, [[maybe_unused]] XKeyEvent & xkey) {};
    
  };

  /**
   * Functor used for checking for a window in the event dispatcher.
   */
  class match_window
  {
  private:
    window * w;

  public:
    match_window(window * p) : w(p) {}
    bool operator()(window * p) { return *w == *p; }
  };

  /**
   * Class that manages sending events to individual windowing windows.
   */
  class event_dispatcher
  {
  private:
    x_display * display;
    bool running = false;
    std::vector<window *> windows;
  public:
    event_dispatcher(x_display * d) : display(d) {}
    
    //! Adds window to the list of windows handled by the event dispatcher.
    void register_window(window * w)
    {
      if (!w) return;
      for (auto vw : windows)
	{
	  if (*vw == *w)
	    return;
	}
      windows.push_back(w);
    }

    //! Removes window from the list of windows handled by the event dispatcher.
    void unregister_window(window * w)
    {
      if (!w) return;
      auto it = std::remove_if(windows.begin(), windows.end(), match_window(w));
      windows.erase(it, windows.end());
    }

    //! Check for windows that are exposed and run their \c on_render function.
    void process_exposed();

    //! Checks for and handles events until event dispatcher is told to stop.
    void run();
    //! Tells event dispatcher to stop processing events.
    void stop() { running = false; }
    //! Handles recieved events based on the type of the event.
    void handle_event(XEvent & event);    
  };

  /**
   * Class designed to handle registering and unregistering a window from an event dispatcher.
   */
  class event_handle
  {
  private:
    window * win = nullptr;
    event_dispatcher * ev_d = nullptr;
  public:
    event_handle(window * win, event_dispatcher * ev_d)
      : win(win), ev_d(ev_d)
    {
      ev_d->register_window(win);
      win->set_dispatcher(ev_d);
    }
    ~event_handle()
    {
      ev_d->unregister_window(win);
      win->set_dispatcher(nullptr);
    }
  };
}


#endif //WINDOWS_HPP
